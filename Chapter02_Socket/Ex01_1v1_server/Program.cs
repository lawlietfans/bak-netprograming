﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Ex01_1v1
{
    class Program
    {
        private static string serverIP = "192.168.3.42";
        private static int serverPort = 8888;
        private static int bufferSize = 1024;
        private static int count = 0;//表示对话序号

        static void Main(string[] args)
        {
            IPAddress ip = IPAddress.Parse(serverIP);
            IPEndPoint ipe = new IPEndPoint(ip, serverPort);
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                s.Bind(ipe);
                s.Listen(10);
                Console.WriteLine("等待连接……");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Thread mainTrd = new Thread(Run);
            mainTrd.Start(s);
        }
        /// <summary>
        /// 启动服务器的socket。
        /// 此处修改为多用户连接，对每个新用户都new一个RecMsg线程
        /// </summary>
        /// <param name="o">传入的socket对象</param>
        private static void Run(object o)
        {
            Socket socket = o as Socket;
            try
            {               
                Socket connSocket = socket.Accept();
                //客户和服务器连接成功。
                Console.WriteLine("{0}成功连接到本机。", connSocket.RemoteEndPoint);
                //接下来的事情交给会话线程
                Thread recTh = new Thread(RecMsg);
                recTh.Start(connSocket);
                Thread sendTh = new Thread(SendMsg);
                sendTh.Start(connSocket);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        private static void RecMsg(object o)
        {
            Socket connSocket = o as Socket;
            while (true)
            {
                byte[] buffer = new Byte[bufferSize];
                try
                {
                    int length = connSocket.Receive(buffer);
                    byte[] realBuffer = new Byte[length];
                    Array.Copy(buffer, 0, realBuffer, 0, length);
                    string str = System.Text.Encoding.Default.GetString(realBuffer);
                    Console.Write("[{0}] ", count++);
                    Console.WriteLine("{0}说：{1}.", connSocket.RemoteEndPoint, str);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.ReadKey();
                    break;
                }                            
            }
        }
        private static void SendMsg(object o) 
        {
            Socket connSocket = o as Socket;
            while (true)
            {
                string str = Console.ReadLine();
                if (str != string.Empty)
                {
                    byte[] buffer = Encoding.Default.GetBytes(str);
                    connSocket.Send(buffer, buffer.Length, SocketFlags.None);
                }               
            }
        }
    }
}

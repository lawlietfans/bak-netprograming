﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;

namespace Ex02_wpfSpider
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        string location = "九寨沟";
        string url = "http://www.tuniu.com/guide/v-jiuzhaigou-23/#cat_163";
        string info = "";
        int[] result = new int[3] { -1, -1, -1 };
        string keyword;
        //数据库
        public MainWindow()
        {
            InitializeComponent();
            string connectionStringLocal =
"Database='mydb';Data Source='127.0.0.1';User Id='lawlietfans';Password='lawlietfans';charset='utf8';pooling=true";
            MySqlHelper.Conn = connectionStringLocal;
            this.dg_tuniu_get();
            this.tabControl.SelectedIndex = 0;
            //设置默认值
            this.tb_link.Text = url;
            this.tb_location.Text = location;                  
        }

        private void btn_link_Click(object sender, RoutedEventArgs e)
        {
            //都不为空的情况下使用新值
            if (this.tb_link.Text != string.Empty && this.tb_location.Text != string.Empty)
            {
                url = this.tb_link.Text;
                location = this.tb_location.Text;
            }          
            DoWork worker = new DoWork();
            info = worker.getHTMLbyWebRequest(url, out result);
            keyword = string.Format("满意度：{0}\n关注者：{1}\n评论数：{2}", result[0], result[1], result[2]);
            this.rtb_info.AppendText("\n"+info);
            this.textBlock_html.Text = keyword;
            this.status_lbl.Content = keyword.Replace('\n', ' ');
            this.btn_insert.IsEnabled = isOk();
        }
        /// <summary>
        /// 检测当前的数据是否有效
        /// </summary>
        private bool isOk()
        {
            foreach (int i in result)
            {
                if (i < 0) return false;
            }
            return true;
        }

        private void btn_insert_Click(object sender, RoutedEventArgs e)
        {
            string sql =
string.Format("insert into tb_tuniuData(location,url,satpercent,followers,comments) values({0},\"{1}\",{2},{3},{4})",
        location, url, result[0], result[1], result[2]);
            dg_tuniu_insert(sql);
        }
        private void dg_tuniu_get()
        {
            string sql = "select * from tb_tuniuData";
            try
            {
                DataSet ds = MySqlHelper.GetDataSet(MySqlHelper.Conn, CommandType.Text, sql, null);
                //this.dg_tuniu.DataSource = ds.Tables[0];//Winform中的用法
                this.dg_tuniu.ItemsSource = ds.Tables[0].DefaultView;
                Debug.WriteLine("查询成功");
            }
            catch (Exception ex)
            {
                
                Debug.WriteLine(ex);
            }
            
        }
        private void dg_tuniu_insert(string sql)
        {
            Debug.WriteLine(sql);
            try
            {
                int result = MySqlHelper.ExecuteNonQuery(MySqlHelper.Conn, CommandType.Text, sql, null);
                if (result != 0)
                {
                    status_lbl.Content="添加成功。";
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                status_lbl.Content = "入库失败，请重试。";
            }
        }

        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            this.dg_tuniu_get();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//add
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Diagnostics;
///正则表达式抓取不到东西
///csdn：http://blog.csdn.net/cyh_24/article/details/7989790
///
namespace Ex02_wpfSpider
{
    class DoWork
    {
        public string getHTMLbyWebRequest(string strUrl, out int[] keywords)
        {
            string info = "页面的描述信息。";
            int[] ks = new int[3] { -1, -1, -1 };//out类型在离开函数前必须赋值
            Encoding encoding = System.Text.Encoding.Default;
            WebRequest request = WebRequest.Create(strUrl);
            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                info = ex.ToString();
                keywords = ks;//数组赋值
                return info;
            }
            
            if (response.StatusDescription.ToUpper() == "OK")
            {
                switch (response.CharacterSet.ToLower())
                {
                    case "gbk":
                        encoding = Encoding.GetEncoding("GBK");//貌似用GB2312就可以
                        break;
                    case "gb2312":
                        encoding = Encoding.GetEncoding("GB2312");
                        break;
                    case "utf-8":
                        encoding = Encoding.UTF8;
                        break;
                    case "big5":
                        encoding = Encoding.GetEncoding("Big5");
                        break;
                    case "iso-8859-1":
                        encoding = Encoding.UTF8;//ISO-8859-1的编码用UTF-8处理，致少优酷的是这种方法没有乱码
                        break;
                    default:
                        encoding = Encoding.UTF8;//如果分析不出来就用的UTF-8
                        break;
                }
                //显示页面简单信息
                info = string.Format("Length: {0}\nCharacterSet:{1}\nHeaders:{2}.",
                    response.ContentLength.ToString(), response.CharacterSet, response.Headers);
                
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream, encoding);
                string html = reader.ReadToEnd();
                    //显示出抓取到的全部html源码。
                    //Console.WriteLine(html);

                //筛选出需要的文本                
                
                Debug.WriteLine("开始调用正则函数");
                FindKeyword(html, out ks[0], out ks[1], out ks[2]);
                
                reader.Close();
                dataStream.Close();
                response.Close();
            }
            else
            {
                info = "获取失败";
                Debug.WriteLine("没有获取到HTML源码。");
            }
            keywords = ks;//数组赋值
            return info;
        }
        private string FindKeyword(string html)
        {
            Debug.WriteLine("==========================");
            string keyword;
            //获取符合规则的集合
            string regex1 = "<span>[\\w]+?</span><em.+?>[\\s]+(?<grp0>[^\\D]+)[\\s]+</em><i>";
            string regex2 = "<p><i .+?>(?<grp0>[^\\D]+)</i>(?<grp1>[\\w]+?)</p>";
            string regex3 = "<p><i.+?>[\\s]+(?<grp0>[^\\D]+)[\\s]+</i>(?<grp1>[\\w]+?)</p>";
            //MatchCollection mc = Regex.Matches(html, pattern);
            Match match = Regex.Match(html, regex3);
            Debug.WriteLine("groups[]：" + match.Groups["grp0"].Value);
            if (match.Success)
            {
                keyword = match.Groups[1].ToString();
            }
            else
            {
                keyword = "获取失败";
            }
            Debug.WriteLine("抓取到的结果："+keyword);
            return keyword;
        }
        /// <summary>
        /// 得到满意度、关注者和评论三个指标。
        /// </summary>
        /// <param name="html">html源码</param>
        /// <param name="satpercent"></param>
        /// <param name="followers"></param>
        /// <param name="comments"></param>
        private void FindKeyword(string html,out int satpercent,out int followers,out int comments)
        {
            Debug.WriteLine("检索出一组关键字：");
                //一次检测出三组数据的正则表达
                //<span>[\w]+?</span><em.+?>[^\d]+(?<grp0>[^\D]+)[\s]+</em><i>[\S\s]+?<p><i.+?>(?<grp1>[^\D]+)</i>[\S\s]+?<p><i.+?>[^\d]+(?<grp2>[^\D]+)[\s]+</i>
            //获取符合规则的集合
            string regex1 = "<span>[\\w]+?</span><em.+?>[\\s]+(?<grp0>[^\\D]+)[\\s]+</em><i>";
            string regex2 = "<p><i .+?>(?<grp0>[^\\D]+)</i>(?<grp1>[\\w]+?)</p>";
            string regex3 = "<p><i.+?>[\\s]+(?<grp0>[^\\D]+)[\\s]+</i>(?<grp1>[\\w]+?)</p>";
            List<string> regexs = new List<string>(); int[] shit = new int[3]; int i=0;
            regexs.Add(regex1); regexs.Add(regex2); regexs.Add(regex3);
            foreach (string s in regexs)
            {
                Debug.WriteLine(s);
                Match m = Regex.Match(html, s);
                if (m.Success)
                {
                    shit[i++] = Convert.ToInt32(m.Groups[1].ToString());
                }
                else
                {
                    Debug.WriteLine("shit[{0}]获取失败。",i);
                }
            }
            Debug.WriteLine("满意度：{0},关注者{1}，评论数{2}", shit[0], shit[1], shit[2]);
            satpercent = shit[0];
            followers = shit[1];
            comments = shit[2];
        }
       
        /// <summary>
        /// 通过URL分析IP地址的方法.
        /// 有些域名做均衡负载的都可以分析出多个IP，不过只能在本地运行
        /// </summary>
        /// <param name="url"></param>
        public void IPAddresses(string url)
        {
            url = url.Substring(url.IndexOf("//") + 2);
            if (url.IndexOf("/") != -1)
            {
                url = url.Remove(url.IndexOf("/"));
            }
                //this.Literal1.Text += "<br>" + url;
            string strTmp = "\n" + url;
            try
            {
                System.Text.ASCIIEncoding ASCII = new System.Text.ASCIIEncoding();
                IPHostEntry ipHostEntry = Dns.GetHostEntry(url);
                System.Net.IPAddress[] ipaddress = ipHostEntry.AddressList;
                foreach (IPAddress item in ipaddress)
                {
                        //this.Literal1.Text += "<br>IP:" + item;
                    strTmp += "\nIP: " + item;
                }
                Console.WriteLine(strTmp);
                /*输出结果
                 *  www.tuniu.com
                    IP: 58.68.255.45
                 */
            }
            catch (Exception ex)
            {
                Console.WriteLine("分析IP出现异常："+ex );
            }
        }
    }
}

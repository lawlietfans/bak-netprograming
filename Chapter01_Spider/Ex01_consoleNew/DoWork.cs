﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//add
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Sockets;
///正则表达式抓取不到东西
///csdn：http://blog.csdn.net/cyh_24/article/details/7989790
///
namespace Ex01_consoleNew
{
    class DoWork
    {
        public void getHTMLbyWebRequest(string strUrl, string pattern, out string keyword)
        {
            Encoding encoding = System.Text.Encoding.Default;
            WebRequest request = WebRequest.Create(strUrl);
            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusDescription.ToUpper() == "OK")
            {
                switch (response.CharacterSet.ToLower())
                {
                    case "gbk":
                        encoding = Encoding.GetEncoding("GBK");//貌似用GB2312就可以
                        break;
                    case "gb2312":
                        encoding = Encoding.GetEncoding("GB2312");
                        break;
                    case "utf-8":
                        encoding = Encoding.UTF8;
                        break;
                    case "big5":
                        encoding = Encoding.GetEncoding("Big5");
                        break;
                    case "iso-8859-1":
                        encoding = Encoding.UTF8;//ISO-8859-1的编码用UTF-8处理，致少优酷的是这种方法没有乱码
                        break;
                    default:
                        encoding = Encoding.UTF8;//如果分析不出来就用的UTF-8
                        break;
                }
                string responseDescription = string.Format("Length: {0}\nCharacterSet:{1}\nHeaders:{2}.",
                    response.ContentLength.ToString(), response.CharacterSet, response.Headers);
                //显示页面简单信息
                Console.WriteLine(responseDescription);

                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream, encoding);
                string html = reader.ReadToEnd();
                //显示出抓取到的全部html源码。
                //Console.WriteLine(html);
                    //FindLink(responseFromServer);//筛选出需要的文本
                    //this.TextBox2.Text = ClearHtml(responseFromServer);
                //FindLink(html);//此句抓取方式没问题。
                //筛选出需要的文本                
                keyword = FindKeyword(html, pattern);
                
                reader.Close();
                dataStream.Close();
                response.Close();
            }
            else
            {
                keyword = "获取失败";
                Console.WriteLine("没有获取到HTML源码。");
            }
        }
        private string FindKeyword(string html, string pattern)
        {
            Console.WriteLine("==========================");
            string keyword;
            //获取符合规则的集合
            pattern = "satpercent.+?data-num=\"(.+?)\"";
            pattern = @"satpercent=(satpercent.+?data-num=(.+?))";
            string regex1 = "<span>[\\w]+?</span><em.+?>[\\s]+(?<grp0>[^\\D]+)[\\s]+</em><i>";
            string regex2 = "<p><i .+?>(?<grp0>[^\\D]+)</i>(?<grp1>[\\w]+?)</p>";
            string regex3 = "<p><i.+?>[\\s]+(?<grp0>[^\\D]+)[\\s]+</i>(?<grp1>[\\w]+?)</p>";
            //MatchCollection mc = Regex.Matches(html, pattern);
            Match match = Regex.Match(html, regex3);
            Console.WriteLine("groups[]：" + match.Groups["grp0"].Value);
            if (match.Success)
            {
                keyword = match.Groups[1].ToString();
            }
            else
            {
                keyword = "获取失败";
            }
            Console.WriteLine("抓取到的结果："+keyword);
            return keyword;
        }
        /// <summary>
        /// 找到源码中所有的链接以及其对应的名称
        /// </summary>
        /// <param name="html"></param>
        private void FindLink(string html)
        {
            Console.WriteLine("==========================");
                //this.TextBox3.Text = "";
            string strTmp = "";
            List<string> hrefList = new List<string>();//链接
            List<string> nameList = new List<string>();//链接名称

            string pattern = @"<a\s*href=(""|')(?<href>[\s\S.]*?)(""|').*?>\s*(?<name>[\s\S.]*?)</a>";
            
            //获取符合规则的集合
            MatchCollection mc = Regex.Matches(html, pattern);
            foreach (Match m in mc)
            {
                if (m.Success)
                {
                    //加入集合数组
                    hrefList.Add(m.Groups["href"].Value);
                    nameList.Add(m.Groups["name"].Value);
                    //this.TextBox3.Text += m.Groups["href"].Value + "|" + m.Groups["name"].Value + "\n";
                    strTmp = string.Format("{0}|{1}.", m.Groups["href"].Value, m.Groups["name"].Value);
                }
                Console.WriteLine(strTmp);
            }
            
        }

        /// <summary>
        /// 通过URL分析IP地址的方法.
        /// 有些域名做均衡负载的都可以分析出多个IP，不过只能在本地运行
        /// </summary>
        /// <param name="url"></param>
        public void IPAddresses(string url)
        {
            url = url.Substring(url.IndexOf("//") + 2);
            if (url.IndexOf("/") != -1)
            {
                url = url.Remove(url.IndexOf("/"));
            }
                //this.Literal1.Text += "<br>" + url;
            string strTmp = "\n" + url;
            try
            {
                System.Text.ASCIIEncoding ASCII = new System.Text.ASCIIEncoding();
                IPHostEntry ipHostEntry = Dns.GetHostEntry(url);
                System.Net.IPAddress[] ipaddress = ipHostEntry.AddressList;
                foreach (IPAddress item in ipaddress)
                {
                        //this.Literal1.Text += "<br>IP:" + item;
                    strTmp += "\nIP: " + item;
                }
                Console.WriteLine(strTmp);
                /*输出结果
                 *  www.tuniu.com
                    IP: 58.68.255.45
                 */
            }
            catch (Exception ex)
            {
                Console.WriteLine("分析IP出现异常："+ex );
            }
        }
    }
}
